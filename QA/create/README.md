# General information
Macroses from this directory are aimed to create ROOT Objects for quality assurance of STAR dataset.

# picoRunIdQA.C
Data type: StPicoDst.  
Goal: to build TProfiles of Events and Tracks base characteristics.
## Preparation
### Libraries  
1. StRefMultCorr  
Only if you want to exclude bad runs from StRefMultCorr package. Nothing like centrality is processing.  
Check out a copy from [CVS STAR system](https://drupal.star.bnl.gov/STAR/comp/sofi/tutorials/cvs): cvs co StRoot/StRefMultCorr  
2. StPicoDst  
Use your own way to get it :)  
### Code changes  
- Replace runId range by valid values. It is better for plotting to set minimum (maximum) value 1K less (more) than true value:
[minRundId-1000; maxRunId+1000]
```cpp
Int_t runIdRange[2] = {12125079, 12172016}; 
```
- Replace trigger collection by that you are interested in:  
```cpp
  // Set trigger of interest
  std::vector<unsigned int> triggers;
  triggers.push_back(350003);
  //...
```
- Define or comment out StRefMultCorr usage. See **Libraries** section for details.
```cpp
#define STREFMULTCORR_USAGE
```

### Arguments to pass  
- picoDst file or file with list of picoDst files
```cpp
const Char_t *inFile = "st_physics_12156021_raw_4040004.picoDst.root" 
```
- ROOT file to save all TProfiles 
```cpp
const Char_t *oFileName = "oPicoRunIdQA.root"
```
