# General information
Macroses from this directory are aimed to process ROOT Objects for quality assurance of STAR dataset.

# picoRunIdQA.C
Data type: ROOT file with TProfiles named as hEventProfile_# or hTrackProfile_#, where '#' is index number.    
Goal: to define bad runs, visualize, and save their IDs. Processing is based on content (value) and error variation.  
## Preparation
### Code changes  
- Uncomment following line to print your name on each plot in format '#Author name#, NRNU MEPhI'
```cpp
signature(authorName)->Draw();
```  
**Optional**. Certain TProfile sometimes needs individual approach for visualizing. Put TProfile's name in `if` clause for better drawing. Possible points for changes:  
- `void drawSelection(...)`:  
```cpp
// Range to draw in
double maxYprint = TMath::Max((meanVal+nSigmasMean*sigma),maxVal)+2.3*sigma;
double minYprint = TMath::Min((meanVal-nSigmasMean*sigma),minVal)-2.3*sigma;
// Set range for certain TProfile
if (name.Contains("hEventProfile_2")) maxYprint+=3.*sigma;
prof->GetYaxis()->SetRangeUser(minYprint, maxYprint);
```  
- `TLegend* legendToDraw(...)`:  
```cpp
TLegend* legend;
// Set position for certain TProfile
if (name.Contains("hEventProfile_6"))  legend = new TLegend(0.55,0.28,0.69,0.38);
else legend = new TLegend(0.23,0.78,0.37,0.88);
```  
### Arguments to pass  
- File with TProfiles to process
```cpp
TString inFile = "~/Maker/QA/AuAu200/auau200_qa_runid.root"
```  
- Short colliding system identificator. Better to use one word without separator characters.
```cpp
TString systemShort = "AuAu200"
```  
- Full colliding system name including energy
```cpp
TString system = "Au+Au #sqrt{s_{NN}} = 200 GeV", 
```  
- Plots' author name. Prints on each plot '#Author name#, NRNU MEPHI' if `TText* signature(..,)` is uncommented in `void drawSelection(...)` function. See **Code changes** section.
```cpp
TString authorName = "My name", 
```  
- Number of sigmas to restrict runs by TProfile content (value)
```cpp
UInt_t nSigmasMean = 4 
```  
- Number of sigmas to restrict runs by TProfile error
```cpp
UInt_t nSigmasErr = 3
```  
- Flag of zero bins (N(entries) != 0 & content = 0) plotting. In any case such bins (runs) are considered as bad.
```cpp
bool zeroPlot = false
```  
